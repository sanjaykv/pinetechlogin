<?php
session_start();
include_once("class.user.php");
// include_once 'include/class.user.php';
$user = new User();

if (isset($_REQUEST['submit'])) {
    extract($_REQUEST);
    $login = $user->check_login($email, $password);
    if ($login == "true") {
        echo "Login Successful";
        header("location: home.php");
        // header("location:home.php");
    } else {
        echo "error: Invalid Email or Password";
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.javascript"></script>
</head>
<style>
    #container {
        width: 400px;
        margin: 0 auto;
    }
</style>
<script type="text/javascript" language="javascript">
    function submitlogin() {
        var form = document.login;
        if (form.email.value == "") {
            alert("Enter email");
            return false;
        } else if (form.password.value == "") {
            alert("Enter password.");
            return false;
        }
    }
</script>

<span style="font-family: 'Courier 10 Pitch', Courier, monospace; font-size: 13px; font-style: normal; line-height: 1.5;">
    <div id="container">
</span>
<h1>Login Form</h1>
<form action="" method="post" name="login">
    <table>
        <tbody>
            <tr>
                <th>Email:</th>
                <td><input type="text" name="email" required="" /></td>
            </tr>
            <tr>
                <th>Password:</th>
                <td><input type="password" name="password" required="" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input onclick="return(submitlogin());" type="submit" name="submit" value="Login" /></td>
            </tr>
            <tr>
                <td></td>
                <td><a href="register.php">Sign Up</a></td>
            </tr>
        </tbody>
    </table>
</form>
</div>