<?php
include_once("class.user.php");
$user = new User();
if (isset($_REQUEST['submit'])) {
    extract($_REQUEST);
    $register = $user->register($firstname, $lastname, $dob, $email, $password);
    if ($register == "true") {
        echo '<h2>Registration successful <a href="login.php">Click here</a> to Login</h2>';
    } else {
        echo '<h2>Registration failed. Email already exists !!<h2>';
    }
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<style>
    #container {
        width: 400px;
        margin: 0 auto;
    }
</style>
<script type="text/javascript" language="javascript">
    function submitreg() {
        var form = document.reg;
        if (form.firstname.value == "") {
            alert("Enter first name.");
            return false;
        } else if (form.lastname.value == "") {
            alert("Enter last name.");
            return false;
        } else if (form.dob.value == "") {
            alert("Enter Date Of Birth.");
            return false;
        } else if (form.email.value == "") {
            alert("Enter email.");
            return false;
        } else if (form.password.value == "") {
            alert("Enter password.");
            return false;
        }
    }
</script>
<div id="container">
    <h1>Registration Form</h1>
    <form action="" method="post" name="reg">
        <table>
            <tbody>
                <tr>
                    <th>First Name</th>
                    <td><input type="text" name="firstname" required="" /></td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td><input type="text" name="lastname" required="" /></td>
                </tr>
                <tr>
                    <th>Date Of Birth</th>
                    <td><input type="date" name="dob" required="" /></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><input type="email" name="email" required="" /></td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td><input type="password" name="password" required="" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input onclick="return(submitreg());" type="submit" name="submit" value="Register" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><a href="login.php">Already registered! Click Here!</a></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>