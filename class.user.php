<?php
include "config.php";

class User
{

    public $db;

    public function __construct()
    {

        $db = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.";
            exit;
        }
    }

    public function check_login($email, $password)
    {

        $sql = "SELECT id,password,first_name FROM login WHERE email = '$email'";
        $db = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        $result = mysqli_query($db, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $hashed = $row['password'];
        $first_name = $row['first_name'];
        $id = $row['id'];

        $valid = password_verify($password, $hashed);
        if ($valid) {
            $_SESSION['first_name'] = $first_name;
            $_SESSION['id'] = $id;

            return true;
        } else {
            return false;
        }
    }

    public function register($firstname, $lastname, $dob, $emailid, $password)
    {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        $sql = "SELECT * FROM login WHERE  email='$emailid'";
        $db = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        $result = mysqli_query($db, $sql);
        if ($result->num_rows == 0) {
            $sql1 = "INSERT INTO login SET first_name='$firstname', last_name='$lastname', dob='$dob', email='$emailid', password='$hashed_password'";
            $result = mysqli_query($db, $sql1) or die(mysqli_connect_errno() . "Data cannot inserted");
            return $result;
        } else {
            return false;
        }
    }
    public function get_firstname($uid)
    {
        $sql3 = "SELECT first_name FROM login WHERE id = $uid";
        $db = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        $result = mysqli_query($db, $sql3);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $first_name = $row['first_name'];
        return $first_name;
    }
    public function get_session()
    {
        return $_SESSION['id'];
    }
    public function user_logout()
    {
        $_SESSION['id'] = FALSE;
        session_destroy();
    }
}
